import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";


dotenv.config();

const app: Express = express();

const PORT: string | number = process.env.PORT || 8080;

app.get("/", (req: Request, res: Response) => {
    res.json({message: "Hola desde TypeScript"})
})

app.get("/hello", (req: Request, res: Response) => {
    res.send("Hello World");
})


app.listen(PORT, () => {
    console.log("App listening from http://localhost:"+PORT);
})
