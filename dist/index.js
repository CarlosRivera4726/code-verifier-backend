"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const app = (0, express_1.default)();
const PORT = process.env.PORT || 8080;
app.get("/", (req, res) => {
    res.json({ message: "Hola desde TypeScript" });
});
app.get("/hello", (req, res) => {
    res.send("Hello World");
});
app.listen(PORT, () => {
    console.log("App listening from http://localhost:" + PORT);
});
//# sourceMappingURL=index.js.map