import express, {  Request, Response } from "express";
import { HelloController } from "@/controller/HelloController";
import { LogInfo } from "@/utils/logger";

// Router de express
let helloRouter = express.Router();
// GET http://localhost:8080/api/hello/
helloRouter.route('/').get(
    async (req: Request, res: Response) => {
        // obtener a Query Param
        let name: any = req?.query?.name;
        LogInfo(`[/api/hello/] Query param: ${name}`)
        //Controller instance
        const controller: HelloController = new HelloController();
        const response = await controller.getMessage();

        res.json(response);
    })
