export const LogInfo = (message: string) => {
    console.info(`Info: ${message}`)
}

export const LogSuccess = (message: string) => {
    console.assert(`Success: ${message}`)
}

export const LogWarning = (message: string) => {
    console.warn(`Warning: ${message}`)
}

export const LogError = (message: string) => {
    console.error(`Error: ${message}`)
}
